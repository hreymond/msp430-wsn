/*
 * common.h
 * 
 * Copyright (C) 
 * 		2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */

#ifndef DRIVERS_COMMON_H_
#define DRIVERS_COMMON_H_

typedef signed char int8_t;
typedef unsigned char uint8_t;

typedef short int16_t;
typedef unsigned short uint16_t;

typedef unsigned int size_t;

typedef long int32_t;
typedef unsigned long uint32_t;

#endif /* DRIVERS_COMMON_H_ */
