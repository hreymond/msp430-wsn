# MSP430 WSN setup

This repo contains the files needed to run a wireless sensor network example (base station not included).

It relies on:
- The MSP430FR5969 MPU
- The [NRF24L01+](https://www.mouser.com/datasheet/2/297/nRF24L01_Product_Specification_v2_0-9199.pdf) communication module
- The [ADXl345](https://www.analog.com/media/en/technical-documentation/data-sheets/ADXL345.pdf) accelerometer

## Experimental Setup

The *NRF24L01+* is connected to the MSP430 via the SPI interface B0:

| NRF24L01 | MSP430 |
|----------| -------|
| VCC/GND  | VCC/GND|
| CE       | P1.4   |
| CSN      | P1.5   |
| MOSI     | P1.6   |
| MISO     | P1.7   |
| SCKL     | P2.2   |

The *NRF24L01+* setup is the following:

| Parameter | Value |
|---------- | ------|
| Data rate | 1 Mbps|
| Channel   | 2     |
| Payload   | 1 Byte|
| TxAddr    | EEEEE |
| RxAddr    | FFFFF |
| CRC       | ON, 16bits |
| AutoACK   | ON    |
| Tx Power  | LOW (-12dBM)|

## Setup

To modify and compile the code, open the project into code composer studio (CCS)
