/*
 * main.c
 *
 * Copyright (C)
 *      2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */

#include <msp430.h>
#include "drivers/nrf24.h"
#include "drivers/spi.h"
#include "drivers/adxl345.h"

struct measurement {
    int16_t x;
    int16_t y;
    int16_t z;
    int16_t timestamp;
}; // 8 bytes

void main(void)
    {

    P1DIR |= BIT0 + BIT5;
    PM5CTL0 &= ~LOCKLPM5;
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    struct nrf24_opts_t opts = {
      .tx_address = "EEEEE",
      .rx_address = "FFFFF",
      .payload_size = 8,
      .rf_power = 1
    };

    struct measurement m;

    nrf24_init(&opts);

    char data[10];
    adxl345_init();
    adxl345_read(0x00, data, 1);

    char text[] = "Hello, World !";

    while(1){
        __delay_cycles(900000);
        adxl345_read(0x32, data, 6);

        m.x = ((data[1]<<8)|data[0]);
        m.y = ((data[3]<<8)|data[2]);
        m.z = ((data[5]<<8)|data[4]);
        m.timestamp = 0;
        nrf24_send(&m, sizeof(struct measurement));
    }

    while(1);
}
