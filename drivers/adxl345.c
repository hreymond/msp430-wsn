/*
 * adxl345.c
 *
 * Copyright (C)
 *      2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */

#define DATA_FORMAT 0x31 // Data format register
#define DATA_FORMAT_B 0x0b // ?
#define READ_BIT 0x80 // Read flag, to read a register
#define MULTI_BIT 0x40 // Multi byte flag, to read or write multiple bytes in a single transmission
#define BW_RATE 0x2c
#define POWER_CTL 0x2d // Set in measure mode
#define DATAX0 0x32 // Data register

#include "spi.h"
#include "adxl345.h"

// Define the eUSCI used for the SPI interface
#define SPI_A1

#ifdef SPI_B0
    #define spi_init spi_B0_init
    #define spi_write spi_B0_write
    #define spi_initialized spi_B0_initialized
    #define spi_read spi_B0_read
#endif
#ifdef SPI_A1
    #define spi_init spi_A1_init
    #define spi_write spi_A1_write
    #define spi_initialized spi_A1_initialized
    #define spi_read spi_A1_read
#endif


void adxl345_init() {
    if(!spi_initialized()){
        spi_init();
    }
   // adxl345_write(BW_RATE, 0x0F);
   adxl345_write(DATA_FORMAT, 0x00);
   adxl345_write(POWER_CTL, 0x08);

}

void adxl345_write(char address, char data) {
   char command[2];
   command[0] = address;
   command[0] |= MULTI_BIT;
   command[1] = data;
   spi_write(command, 2);
}


void adxl345_read(char address, char * data, int data_len) {
    char command[8];
    command[0] = address | READ_BIT | MULTI_BIT;
    command[1] = 0x00;
    spi_read(command, data, data_len+1);
}

//void adxl345_measure() {
//    DATAX0
//}

/***
 *
    read_data(READ_DATA, data, 8);

 *
 */
