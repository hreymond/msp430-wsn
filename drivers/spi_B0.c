/*
 * spi.c
 *
 * Copyright (C)
 *      2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */
#include "msp430.h"
#include "gpio.h"

/*
 * SPI communication driver for MSP430FR5969 using Enhanced Universal Serial Communication Interface eUSCI_B
 *
 * MISO: P1.7 UCB0SOMI
 * MOSI: P1.6 UCB0SIMO
 * SCLK: P2.2 UCB0CLK
 * CS: P1.2
 *  */
#define SPI_B0_CS BIT5

static int driver_initialized = 0;

void spi_B0_init() {
    /*** Initialize SPI module for the following:
     *
     * eUSCI_B0 module
     *
     * MISO: P1.7 UCB0SOMI
     * MOSI: P1.6 UCB0SIMO
     * SCLK: P2.2 UCB0CLK
     * CS: P1.5
     */
    // Set CS pin
    P1OUT |= SPI_B0_CS;
    P1DIR |= SPI_B0_CS;



    // Set pin modes to USCI
    // MISO & MOSI
    P1SEL1 |= BIT7 + BIT6;
    P1SEL0 &= ~(BIT7 + BIT6 );
    // CLK
    P2SEL1 |= BIT2;
    P2SEL0 &= ~(BIT2);


    // Reset the USCI module
    UCB0CTLW0 = UCSWRST;

    // Set:
    // UCCKPH -> CLOCK_PHASE = 0,
    // UCCKPL -> CLOCK_POLARITY = 1,
    // UCMSB -> 0b = LSB first, 1b = MSB first
    // UCMST -> Master mode select 0=slave, 1=master
    // UCMODEx -> 0 here, since CS is controlled separately
    // UCSYNC = 1 -> Synchronous mode (je ne sais pas ce que cela fait)
    // UCSTEM = 0 -> Use STE as Chip Select

    UCB0CTLW0 |=  UCCKPH + UCMSB + UCMST + UCSSEL__SMCLK + UCSYNC;
    UCB0BRW = 80;
    UCB0CTL1 &= ~UCSWRST;

    driver_initialized = 1;

}


inline int spi_B0_initialized() {
    return driver_initialized;
}

void spi_B0_write(char * bytes, int len) {
   int i;
   P1OUT &= ~SPI_B0_CS;
   for(i=0;i<len;i++){
       UCB0TXBUF = bytes[i];
       while(UCB0STATW & UCBUSY);
   }
   P1OUT |= SPI_B0_CS;
}

void spi_B0_read(char * input, char * output, int len) {
    int i;
    P1OUT &= ~SPI_B0_CS;
    for(i=0;i<len;i++){
        UCB0TXBUF = input[i];
        while(UCB0STATW & UCBUSY);
        if(i>0) {
            output[i-1] = UCB0RXBUF;
        } else {
            output[i] = UCB0RXBUF;
        }
    }
    P1OUT |= SPI_B0_CS;
}


