/*
 * spi.h
 *
 * Copyright (C)
 *      2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */

#ifndef DRIVERS_SPI_H_
#define DRIVERS_SPI_H_

/*** Initialize the SPI interface
 */
void spi_A1_init();
void spi_B0_init();

/*** Return 1 if the SPI interface has been initialized, 0 else
 */
int spi_A1_initialized();
int spi_B0_initialized();

/*** Write a message through the SPI interface
 *
 * eUSCI_B0 with CS = 1.5
 */
void spi_A1_write(char * bytes, int len);
void spi_B0_write(char * bytes, int len);

/*** Read a message from the SPI interface
 *
 */
void spi_A1_read(char * input, char * output, int len);
void spi_B0_read(char * input, char * output, int len);

#endif /* DRIVERS_SPI_H_ */
