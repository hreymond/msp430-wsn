/*
 * adxl345.h
 *
 * Copyright (C)
 *      2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */

#ifndef DRIVERS_ADXL_H_
#define DRIVERS_ADXL_H_

void adxl345_init();
void adxl345_write(char address, char data);
void adxl345_read(char address, char * data, int data_len);

#endif /* DRIVERS_ADXL_H_ */
