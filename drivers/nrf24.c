/*
 * nrf24.c
 *
 * Copyright (C)
 *      2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */
#include "spi.h"
#include "nrf24.h"
#include "msp430.h"

struct nrf24_opts_t * opts;

#define STATIC_PERIH_CONFIG

#ifdef STATIC_PERIH_CONFIG
#define PAYLOAD_SIZE 8
#else
#define PAYLOAD_SIZE opts->payload_size
#endif //STATIC CONFIG

// Define the eUSCI used for the SPI interface
#define SPI_B0

#ifdef SPI_B0
    #define spi_init spi_B0_init
    #define spi_write spi_B0_write
    #define spi_initialized spi_B0_initialized
    #define spi_read spi_B0_read
#endif
#ifdef SPI_A1
    #define spi_init spi_A1_init
    #define spi_write spi_A1_write
    #define spi_initialized spi_A1_initialized
    #define spi_read spi_A1_read
#endif

#define CSN BIT5

void nrf24_init(struct nrf24_opts_t * driver_opts) {
    char command[6];

    if(!spi_initialized()) {
       spi_init();
    }
    P1DIR |= CSN;
    P1OUT &= ~CSN;

    opts = driver_opts;

    nrf24_write_reg(RF_SETUP, opts->rf_power << 1);
    nrf24_write_reg(CONFIG, PWR_UP | CRC0 | EN_CRC );
    nrf24_write_reg(EN_AA, 0x03);
    nrf24_write_reg(RX_PW_P0, PAYLOAD_SIZE);

    // Flush the TX/RX pipes
    command[0] = FLUSH_TX;
    spi_write(command, 1);

    command[0] = FLUSH_RX;
    spi_write(command, 1);

    // Clean all flags
    nrf24_write_reg(STATUS, 0xFF);

//  Debug only, check if the registers are well initialized
//    char data[5];
//    char volatile status = nrf24_read_reg(STATUS);
//    char volatile config = nrf24_read_reg(CONFIG);
//    char volatile en_aa = nrf24_read_reg(EN_AA);
//    char volatile setup_aw = nrf24_read_reg(SETUP_AW);
//    char volatile en_rxadrr = nrf24_read_reg(EN_RXADDR);


    nrf24_write_multi_reg(TX_ADDR, opts->tx_address, 5);
    nrf24_write_multi_reg(RX_ADDR_P0, opts->tx_address, 5);
    nrf24_write_multi_reg(RX_ADDR_P1, opts->rx_address, 5);

//    char tx_addr_read[8];
//    char rx_addr_read[8];
//    nrf24_read_multi_reg(TX_ADDR, tx_addr_read, 5);
//    nrf24_read_multi_reg(RX_ADDR_P0, rx_addr_read, 5);



}

/*** Send data via RF with the nRF24
 *
 * The payload will be split into multiples packet if it is bigger than the payload_size
 */
void nrf24_send(char * payload, size_t length){
    int i;
    const uint8_t message_size = PAYLOAD_SIZE;
    // Compute how many full packet can be sent
    uint8_t nb_messages = length/message_size;
    for(i =0; i < nb_messages; i++){
        nrf24_send_packet_payload_size(payload);
        payload += 4;
    }

    // Sent the last (incomplete) packet, if necessary
    int bytes_lefts = length % message_size;
    if(bytes_lefts) {
        nrf24_send_packet(payload, bytes_lefts);
    }

}

char data[MAX_PAYLOAD + 1];

/*** Send an RF packet via the nRF24
 *
 * The payload size must be inferior to the maximum packet size
 */
void inline nrf24_send_packet(char * payload, size_t length){
    int j;
    data[0] = W_TX_PAYLOAD;
    // Prepare the spi payload, fill empty space with 0 to fit packet size
    for(j = 0; j< PAYLOAD_SIZE; payload++, j++) {
      if(j < length) {
          data[j+1] = *payload;
      } else {
          data[j+1] = 0;
      }
    }
    // Send the spi message
    spi_write(data, PAYLOAD_SIZE + 1);
    P1OUT |= CSN;
    __delay_cycles(DELAY_RXTX_CYCLES);
    P1OUT &= ~CSN;
    data[0] = FLUSH_TX;
    spi_write(data, 1);
}

/*** Send a RF packet via the nRF24
 *
 * /!\ Warning: The payload size must fit the payload_size parameter. If it is bigger, the data above
 * payload_size won't be sent. If it is smaller, will cause unplanned memory accesses
 *
 */
void inline nrf24_send_packet_payload_size(char * payload){
    int j;
    data[0] = W_TX_PAYLOAD;
    uint8_t message_size = PAYLOAD_SIZE;
    for(j = 0; j < message_size; j++, payload++) {
              data[j+1] = *payload;
    }
    spi_write(data, 1 + message_size);
    P1OUT |= CSN;
    __delay_cycles(DELAY_RXTX_CYCLES);
    P1OUT &= ~CSN;
    data[0] = FLUSH_TX;
    spi_write(data, 1);
}

/*** Write multiples values into a nrf24 register (used to set nRF24 parameters as TX/RX addresses)
 *
 */
void nrf24_write_multi_reg(char address, char * data, int data_len) {
   char command[10];

   command[0] = address | W_REGISTER;
   int i = 1;
   for(; i <= data_len; i++) {
       command[i] = data[data_len-i];
   }
   spi_write(command, data_len + 1);
}


/*** Write into a nrf24 register
 *
 */
void nrf24_write_reg(char address, char data) {
   char command[2];
   command[0] = address | W_REGISTER;
   command[1] = data;
   spi_write(command, 2);
}

/*** Read a nrf24 register
 *
 */
char nrf24_read_reg(char address) {
    char command[2];
    char data;
    command[0] = R_REGISTER | address;
    command[1] = 0x00;
    spi_read(command, &data, 2);

    return data;
}

/*** Read multiples nrf24 register
 *
 */
void nrf24_read_multi_reg(char address, char * data, int data_len) {
    char command[8];
    command[0] = R_REGISTER | address;
    command[1] = 0x00;
    spi_read(command, data, data_len + 1);
}



