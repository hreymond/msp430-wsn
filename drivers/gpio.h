/*
 * gpio.h
 *
 *  Created on: Jul 22, 2022
 *      Author: hreymond
 */

#ifndef DRIVERS_GPIO_H_
#define DRIVERS_GPIO_H_

#define PORTS_OUT [0, P1OUT, P2OUT, P3OUT, P4OUT]

#define OUTPUT 1
#define INPUT 0

#define gpio_on(port, pin) (port |= 1 << pin, 0)
#define gpio_off(port, pin) (port &= ~ (1 << pin), 0)
#define gpio_write(port, pin, val) (val ? ( gpio_on(port, pin)) : (gpio_off(port, pin)))

#define gpio_set(port, pin, val) (val ? ( gpio_on(port, pin)) : (gpio_off(port, pin)))

//void inline port_write(auto port, int pin, int value){
//    if(value) {
//        port |= pin;
//    } else {
//        port ~= pin;
//    }
//}



#endif /* DRIVERS_GPIO_H_ */
