/*
 * spi.c
 *
 * Copyright (C)
 *      2022 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */
#include "msp430.h"
#include "gpio.h"

/*
 * SPI communication driver for MSP430FR5969 using Enhanced Universal Serial Communication Interface eUSCI_A
 *
 * MISO: P2.6 UCA1SOMI
 * MOSI: P2.5 UCA1SIMO
 * SCLK: P2.4 UCA1CLK
 * CS: P1.3
 *  */
#define SPI_A1_CS BIT3

static int driver_initialized = 0;

void spi_A1_init() {
    /*** Initialize SPI module for the following:
     *
     * eUSCI_A1 module
     *
     * MISO: P2.6 UCA1SOMI
     * MOSI: P2.5 UCA1SIMO
     * SCLK: P2.4 UCA1CLK
     * CS: P1.3
     */
    // Set CS pin
    P1OUT |= SPI_A1_CS;
    P1DIR |= SPI_A1_CS;



    // Set pin modes to USCI
    // MISO & MOSI & CLK
    P2SEL1 |= BIT6 + BIT5 + BIT4;
    P2SEL0 &= ~(BIT6 + BIT5 + BIT4);


    // Reset the USCI module
    UCA1CTLW0 = UCSWRST;

    // Set:
    // UCCKPH -> CLOCK_PHASE = 0,
    // UCCKPL -> CLOCK_POLARITY = 1,
    // UCMSB -> 0b = LSB first, 1b = MSB first
    // UCMST -> Master mode select 0=slave, 1=master
    // UCMODEx -> 0 here, since CS is controlled separately
    // UCSYNC = 1 -> Synchronous mode (je ne sais pas ce que cela fait)
    // UCSTEM = 1 -> Use STE as Chip Select

    UCA1CTLW0 |=  UCCKPL + UCMSB + UCMST + UCSSEL__SMCLK + UCSYNC;
    UCA1BRW = 80;
    UCA1CTL1 &= ~UCSWRST;

    driver_initialized = 1;

}


inline int spi_A1_initialized() {
    return driver_initialized;
}

void spi_A1_write(char * bytes, int len) {
   int i;
   P1OUT &= ~SPI_A1_CS;
   for(i=0;i<len;i++){
       UCA1TXBUF = bytes[i];
       while(UCA1STATW & UCBUSY);
   }
   P1OUT |= SPI_A1_CS;
}

void spi_A1_read(char * input, char * output, int len) {
    int i;
    P1OUT &= ~SPI_A1_CS;
    for(i=0;i<len;i++){
        UCA1TXBUF = input[i];
        while(UCA1STATW & UCBUSY);
        if(i>0) {
            output[i-1] = UCA1RXBUF;
        } else {
            output[i] = UCA1RXBUF;
        }
    }
    P1OUT |= SPI_A1_CS;
}


